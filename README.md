# Demo Feign with Resilience4J circuitbreaker in Spring Boot

## Features demonstrated in this app

* Spring Boot app calling 'external' service
* Declarative Feign client
* Resilience4J circuitbreaker with fallback method

## Project Setup

This project consists of 2 modules which can be build using Maven.
* demo-application - Rest endpoint calling external service (http://localhost:8080)
* demo-service - the 'external service' (http://localhost:8081)

Run `mvn clean install` from the root directory of this project. 
Open your favorite IDE (eg. IntelliJ) and :
   1. Start the 'external service' -> Right-click ServiceApplication-class in demo-service project
   2. Start the application -> Right-click DemoApplication from demo-application project

## Demo 1 Retrieve all products via Feign-annotated HTTP client
-------
### Step 1: add Maven dependency
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-openfeign</artifactId>
            <version>2.2.5.RELEASE</version>
        </dependency>

### Step 2: Create an interface for calling external service 
Add an interface for calling your service:

```
@Headers("Content-Type: application/json")
@FeignClient(value = "demoServiceClient", url = "http://${demo.service.hostName}:${demo.service.portNumber}")
public interface DemoServiceClient {

    @RequestMapping(method = RequestMethod.GET, value="/products")
    List<ProductResponse> getProducts(); 
```
Annotate the interface with @FeignClient 2 parameters:
* 'value' - name of the HTTP client configuration in application.yml file 
* 'url'   - the url to the external service 

See [application.yml ](https://gitlab.com/craftsmen/resilience4j-demo/-/blob/master/demo-application/src/main/resources/application.yml) for the HTTP configuration (check demoServiceClient)

### Step 3: Enable Feign on Spring Boot application
Add the @EnableFeignClients on your @SpringBootApplication annotated class

### Step 4: Retrieve all products from demo-application
Open url http://localhost:8080/products and check whether all products will be returned.

## Demo 2 Configure Circuitbreaker for external service
-------
### Step 1: add Maven dependencies
The circuitbreaker itself

        <dependency>
            <groupId>io.github.resilience4j</groupId>
            <artifactId>resilience4j-circuitbreaker</artifactId>
            <version>1.6.1</version>
        </dependency>

The annotation’s library for using in the client

        <dependency>
            <groupId>io.github.resilience4j</groupId>
            <artifactId>resilience4j-annotations</artifactId>
            <version>1.6.1</version>
        </dependency>

Spring boot for support in application.yml

        <dependency>
            <groupId>io.github.resilience4j</groupId>
            <artifactId>resilience4j-spring-boot2</artifactId>
            <version>1.6.1</version>
        </dependency>

### Step 2: add method call to interface 

```
    @RequestMapping(method = RequestMethod.GET, value = "/product/{id}")
    @CircuitBreaker(name = "demoServiceCircuitBreaker", fallbackMethod = "demoServiceFallbackMethod")
    ProductResponse getProduct(@PathVariable("id") Integer id);

```
Annotate the new method with @CircuitBreaker and two parameters:
* 'name'            - name of the Resilience4J configuration in application.yml file 
* 'fallbackMethod'  - the name of the fallback method 

See [application.yml ](https://gitlab.com/craftsmen/resilience4j-demo/-/blob/master/demo-application/src/main/resources/application.yml) for the Resilience4J configuration (check demoServiceCircuitBreaker)

Check all the individual properties for configuring the circuitbreaker.
Also be aware of the fact the we can use a baseConfig for all our services and overwrite
specific values when needed (to avoid duplication)

### Step 3: define fallback method
Add the fallback-method with the same method signature as the original method but with one extra argument (Exception).
Make sure the name of the method is equal to the one you defined at the fallbackMethod-parameter.

```
    default ProductResponse demoServiceFallbackMethod(Integer id, Exception exc) {
        LOGGER.error("Got an error, executing fallbackmethod and returning default from application");
        return DemoServiceClient.defaultProduct();
    }
``` 

### Step 4: Retrieve product details and see the circuitbreaker in action
Open url http://localhost:8080/all and see that our default product (free Coffee!!) is returned.
Make sure to check the log for the demo-service and see that our demo-service is only called twice,
although our demo-application called it 3 times. The circuitbreaker prevented the third call
due to too much errors.
