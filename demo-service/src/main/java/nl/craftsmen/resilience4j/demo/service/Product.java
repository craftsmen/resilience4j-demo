package nl.craftsmen.resilience4j.demo.service;

public class Product {
    public final int id;
    public final String name;
    public final double price;

    public Product(int id, String name, double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

}
