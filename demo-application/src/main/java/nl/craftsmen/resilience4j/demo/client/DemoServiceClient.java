package nl.craftsmen.resilience4j.demo.client;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import feign.Headers;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@Headers("Content-Type: application/json")
@FeignClient(value = "demoServiceClient", url = "http://${demo.service.hostName}:${demo.service.portNumber}")
public interface DemoServiceClient {
    Logger LOGGER =  LoggerFactory.getLogger(DemoServiceClient.class);

    @RequestMapping(method = RequestMethod.GET, value="/products")
    List<ProductResponse> getProducts();

    @RequestMapping(method = RequestMethod.GET, value = "/product/{id}")
    @CircuitBreaker(name = "demoServiceCircuitBreaker", fallbackMethod = "demoServiceFallbackMethod")
    ProductResponse getProduct(@PathVariable("id") Integer id);

    default ProductResponse demoServiceFallbackMethod(Integer id, Exception exc) {
        LOGGER.error("Got an error, executing fallbackmethod and returning default from application");
        return DemoServiceClient.defaultProduct();
    }

    class ProductResponse {
        private int id;
        private String name;
        private double price;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        @Override
        public String toString() {
            return "ProductResponse{" + "id='" + id + '\'' + ", name='" + name + '\'' + '}';
        }
    }

    static ProductResponse defaultProduct() {
        final ProductResponse productResponse = new ProductResponse();
        productResponse.setId(999);
        productResponse.setName("Free coffee");
        productResponse.setPrice(0.0);
        return productResponse;
    }


}
